<?php

$rute = trim($_SERVER['REQUEST_URI'], '/');
$rute = explode('/', $rute);
loadController($rute);

function loadController($rute) {
    $file = ($rute[0]) ? 'controller_' . $rute[0] . '.php' : 'controller_main.php';
    $controllerClass = ($rute[0]) ? ucfirst($rute[0]) : 'Main';
    $action = ($rute[1]) ? $rute[1] : 'index';

    if (!file_exists($file)) {
        echo "Error 404";
    } else {
        include_once "$file";
    }
    $controller = new $controllerClass;
    if (!method_exists($controller, $action)) {
        echo "Error 404/1";
    } else {
        $controller->$action();
    }
}