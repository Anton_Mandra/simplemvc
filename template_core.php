<?php

class template {

    private $templateContent;

    /*
     * Sets data private $templateContent;
     *
     * @param string $fileName name of template file
     *
     * @return mixed
     */

    public function __construct(string $fileName) {
        $path = './templates/' . $fileName . '.tpl';
        if (file_exists($path)) {
            $this->templateContent = file_get_contents($path);
        } else {
            throw new Exception('Template not found');
        }
    }

    /*
     * Gets data from private $templateContent;
     *
     * @param none
     *
     * @return string
     */

    public function get_templateContent() {
        return $this->templateContent;
    }

    /*
     * Change data in private $templateContent;
     *
     * @param none
     *
     * @return svoid
     */

    public function change_templateContent(string $string) {
        $this->templateContent = $string;
    }

    /*
     * input variables into template;
     *
     * @param array
     *
     * @return string
     */

    public function tpl_input_var(array $array) {
        foreach ($array as $key => $value) {
            $str = $this->get_templateContent();
            $pattern = '/{{' . $key . '}}/u';
            $str = preg_replace($pattern, $value, $str);
        }
        $this->change_templateContent($str);
    }

    /*
     * interpretate IF-ELSE statment, works only with equal (=);
     *
     * @param array,string
     *
     * @return string
     */

    public function interpratete_if($array) {
        $str = $this->get_templateContent();
        $pattern = '/IF(.*?)ENDIF/u';

        if (preg_match($pattern, $str)) {
            preg_match_all($pattern, $str, $matches);

            for ($i = 0, $j = count($matches[0]) - 1; $i <= $j; ++$i) {
                preg_match('/\((.*)\)/u', $matches[1][$i], $conditions);
                preg_match('/\)(.*)ELSE/u', $matches[1][$i], $true);
                preg_match('/ELSE(.*)/u', $matches[1][$i], $false);
                preg_match('/(.*)=/u', $conditions[1], $varName);
                preg_match('/=(.*)/u', $conditions[1], $varValue);

                foreach ($array as $key => $value) {

                    if ($key == $varName[1]) {

                        if ($value == $varValue[1]) {
                            $str = (str_replace($matches[0][$i], $true[1], $str));
                        } else {
                            $str = (str_replace($matches[0][$i], $false[1], $str));
                        }
                    }

                    $this->change_templateContent($str);
                }
            }
        } else {
            return false;
        }
    }

    /*
     * interpretate variables in file.tpl;
     *
     * @param array,string
     *
     * @return string
     */

    public function tpl_parse(array $array) {
        $str = $this->get_templateContent();
        foreach ($array as $key => $value) {
            $pattern = '/{{' . $key . '}}/u';
            $str = preg_replace($pattern, $value, $str);
        }
        $this->change_templateContent($str);
        return true;
    }

    public function interpratete_if2($array) {
        $str ='?>'.$this->get_templateContent();
        $pattern = '/IF(.*?)ENDIF/u';
        extract($array);
        if (preg_match($pattern, $str)) {
            preg_match_all($pattern, $str, $matches);

            for ($i = 0, $j = count($matches[0]) - 1; $i <= $j; ++$i) {
                preg_match('/\((.*)\)/u', $matches[1][$i], $conditions);
                preg_match('/\)(.*)ELSE/u', $matches[1][$i], $true);
                preg_match('/ELSE(.*)/u', $matches[1][$i], $false);
                preg_match('/(.*)=/u', $conditions[1], $varName);
                preg_match('/=(.*)/u', $conditions[1], $varValue);

                $ifstatment = "<?php if ($$varName[1]==$varValue[1]){echo \"$true[1]\";}else{echo \"$false[1]\";}?>";
                $str = str_replace($matches[0][$i], $ifstatment, $str);
                
            }
            eval($str);
        } else {
            return false;
        }
    }

}
