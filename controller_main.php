<?php

class Main {

    public function index() {
        $data['x'] = 5;
        $data['y'] = 4;
        $data['random'] = $this->loadModel('random', 'rand');
        $this->load_temple($data, 'templ');
    }

    public function view() {
        $rand = $this->loadModel('random', 'rand');
        $this->loadView('index', $rand);
    }

    public function hello() {
        echo 'Hello';
    }

    private function loadModel($model, $method_name) {
        $file = 'model_' . $model . '.php';
        $model = ucfirst($model);

        if (!file_exists($file)) {
            throw new Exception("Model file not found");
        } else {
            require_once "$file";
        }

        $modelRun = new $model;

        if (!method_exists($model, $method_name)) {
            throw new Exception('There is no such method');
        } else {
            return $modelRun->$method_name();
        }
    }

    private function load_temple($data, $file) {
        $template = 'template_core.php';
        if (!file_exists($template)) {
            throw new Exception("Template core not found");
        } else {
            require_once "$template";
            $page = new template($file);
            $page->tpl_parse($data);
            $page->interpratete_if2($data);
          
            
           // echo ($page->get_templateContent());
        }
    }

    private function loadView($view, $rand) {
        $file = 'view_' . $view . '.php';
        if (!file_exists($file)) {
            throw new Exception("View file not found");
        } else {
            $pattern = '/\$rand/u';
            $str = file_get_contents($file);
            $page = preg_replace($pattern, $rand, $str);
            echo($page);
        }
    }

}
